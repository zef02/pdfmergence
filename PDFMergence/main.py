__author__ = 'Taichiro Kobayashi'
# -*- coding: utf-8 -*-

import os
import shutil
from pyPdf import PdfFileWriter, PdfFileReader


# 単純PDF結合関数
def SinglePDFmargeMethod(input_A_file,input_B_file, output_file):
    print "concatenation all files:"

    output = PdfFileWriter()
    total_pages = 0

    inputA = PdfFileReader(file(input_A_file, 'rb'))
    inputB = PdfFileReader(file(input_B_file, 'rb'))
    num_pages = inputA.getNumPages()
    for i in xrange(0, num_pages):
        output.addPage(inputA.getPage(i))
    num_pages = inputB.getNumPages()
    for i in xrange(0, num_pages):
        output.addPage(inputB.getPage(i))

    outputStream = file(output_file, 'wb')
    output.write(outputStream)
    print total_pages, "pages written"
    outputStream.close()


# 入力フォルダと出力フォルダを指定し、左から「_00?.pdf」を除いたファイル名が同じファイルであれば結合する
def DirectryPDFmargeMethod(input_dir, output_dir):

    # Inputフォルダ内すべてのファイルを検索
    Targetlist = os.listdir(input_dir)
    for Targetfile in Targetlist:

        # 連番の先頭ファイルでなければスキップ
        if not Targetfile.endswith('_001.pdf'):
            continue

        ## 基本ファイルをコピー
        #shutil.copyfile(input_dir + Targetfile, output_dir + Targetfile)

        # 連番ファイルなのでコピー先に結合
        outputPDF = PdfFileWriter()
        # 基本ファイルを保存
        AlreadyPDF = PdfFileReader(file(input_dir + Targetfile, 'rb'))
        num_pages = AlreadyPDF.getNumPages()
        for i in xrange(0, num_pages):

            outputPDF.addPage(AlreadyPDF.getPage(i))

        # 連番ファイルがあれば基本ファイルに結合していく（ただし同名ファイルを除く）
        for Comparefile in Targetlist:

            # 連番名を除いたファイル名（比較用）
            SplitFileName = Comparefile[: Comparefile.find('.pdf') - 4]

            if Targetfile == Comparefile:
                # 対象ファイルなのでスキップ
                continue

            elif Targetfile.find(SplitFileName) == 0:

                # 今回見つけたPDFをコピー先に結合
                inputPDF = PdfFileReader(file(input_dir + Comparefile, 'rb'))
                num_pages = inputPDF.getNumPages()
                for i in xrange(0, num_pages):
                    outputPDF.addPage(inputPDF.getPage(i))

            else:
                # 関係ないファイルなのでスキップ
                continue

        # 書き込み保存
        outputStream = file(output_dir + Targetfile, 'wb')
        outputPDF.write(outputStream)
        outputStream.close()

TargetDir = "C:\\OTW\\python\\"
#OUTPUTDir = "C:\\OTW\\python\\"
OUTPUTDir = "C:\\Temp\\Sample\\"

# 単純にPDFを結合する関数 ##Sample Code
# SinglePDFmargeMethod(TargetDir + "123456789020140601090000_01.pdf",TargetDir + "123456789020140601090000_02.pdf", OUTPUTDir + "AfterEdit.pdf")


DirectryPDFmargeMethod(TargetDir, OUTPUTDir)


